VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTablaSincro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarNombre                                                                          As String
Private mvarNombrePend                                                                      As String
Private mvarTipo                                                                            As eTablasSincro
Private mCampos                                                                             As Collection
Private mCampoTipoCambio                                                                    As String

Property Get CampoTipoCambio() As String
    Select Case mvarTipo
        Case etblCodigos, etblProductos, etblProductosWeb
            CampoTipoCambio = "TipoCambio"
            
        Case Else
            CampoTipoCambio = "Tipo_Cambio"
    End Select
End Property

Property Get Nombre() As String
    Nombre = mvarNombre
End Property

Property Get NombrePend() As String
    NombrePend = mvarNombrePend
End Property

Property Get CamposEx() As Collection
    Set Campos = mCampos
End Property

Property Get TipoTabla() As eTablasSincro
    TipoTabla = mvarTipo
End Property

Friend Function AgregarTabla(Tipo As eTablasSincro, pNombre As String, pNombrePend As String, pCamposEx)
    mvarNombre = pNombre
    mvarNombrePend = pNombrePend
    mvarTipo = Tipo
    Set mCampos = New Collection
    AgregarCamposEx pCamposEx
    Set AgregarTabla = Me
End Function

Private Sub AgregarCamposEx(pCampos)
    For i = 0 To UBound(pCampos)
        mCampos.Add CStr(pCampos(i))
    Next i
End Sub
