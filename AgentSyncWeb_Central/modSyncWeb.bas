Attribute VB_Name = "modSyncWeb"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Private Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" _
    (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long
    
Private Const ArchivoLog As String = "isAgentSyncWeb.log"
Private Const ArchivoIni As String = "setup.ini"

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

' Advertencia: NO Cambiar ninguno de los 3 par�metros anteriores. Para efectos de resguardo de las
' credenciales de acceso a SQL se esta utilizando la misma combinaci�n de par�metros de Stellar BUSINESS.
' Para que las claves encriptadas sean las mismas en todos los Agentes de Sincronizaci�n Web.
' Despues que las claves hayan sido generadas si esto se llegara a cambiar no funcionar�a la conexi�n SQL
' Al desplegar una nueva versi�n.

Global mUserCnSucursal As String
Global mPassCnSucursal As String

Global mUserCnWeb As String
Global mPassCnWeb As String

Public Function ObtenerConfiguracion(ByVal SSection As String, ByVal SKey As String, ByVal SDefault As String) As String
    Dim sTemp As String * 10000
    Dim nLength As Integer
    
    sTemp = Space$(10000)
    nLength = GetPrivateProfileString(SSection, SKey, SDefault, sTemp, 9999, App.Path & "\" & ArchivoIni)
    ObtenerConfiguracion = Left$(sTemp, nLength)
End Function

Public Sub EscribirConfiguracion(ByVal SSection As String, ByVal SKey As String, ByVal SDefault As String)
    WritePrivateProfileString SSection, SKey, SDefault, App.Path & "\" & ArchivoIni
End Sub

Public Sub EscribirLog(ByVal Msg As String)
    
    Dim mCanal As Integer
    
    On Error Resume Next
    
    mCanal = FreeFile()
    
    Dim TmpArchivo As String
    
    TmpArchivo = App.Path & "\" & ArchivoLog
    
    Open TmpArchivo For Append Access Write As #mCanal
    Print #mCanal, Now & ", " & Msg
    Close #mCanal
    
    Err.Clear
    
End Sub

Sub Main()
    
    If App.PrevInstance Then
        End
    End If
    
    gPK = Chr(55) & Chr(81) & Chr(76) & "_" _
    & Chr(73) & Chr(55) & Chr(51) & Chr(51) _
    & Chr(83) & Chr(78) & Chr(75) & Chr(52) _
    & Chr(78) & Chr(50) & Chr(69) & Chr(74) _
    & Chr(68) & Chr(49) & Chr(83) & "-" _
    & Chr(66) & Chr(44) & Chr(66) & Chr(71) _
    & Chr(52) & Chr(44) & Chr(90) & "_" & Chr(71)
    
    Dim mCls As clsSyncWeb
    
    Set mCls = New clsSyncWeb
    mCls.IniciarAgente
    Set mCls = Nothing
    
    End
    
End Sub

Public Function NombreArchivo(pTabla As String, pCorrelativo As String, Optional pCarpeta As String = "") As String
    
    Dim mDirectorio As String
    
    mDirectorio = IIf(pCarpeta <> "", pCarpeta, App.Path) & "\"
    NombreArchivo = mDirectorio & pTabla & ";" & pCorrelativo & ".adtg"
    'MsgBox NombreArchivo
    
End Function

Public Function BuscarArchivosActualizar(pRutaBusqueda As String, Optional Pedidos As Boolean) As Collection
    
    Dim mArchivos As Collection
    Dim mArchivo As String
    
    Set mArchivos = New Collection
    
    If Pedidos Then
        mArchivo = Dir(pRutaBusqueda & "\RSMP*.adtg")
    Else
        mArchivo = Dir(pRutaBusqueda & "\*pend*.adtg")
    End If
    
    Do While mArchivo <> ""
        mArchivos.Add mArchivo
        mArchivo = Dir
    Loop
    
    Set BuscarArchivosActualizar = mArchivos
    
End Function

Public Function AbrirRsActualizar(pNombre As String, Optional pAsc As Boolean = True) As ADODB.Recordset
    Dim mRs As ADODB.Recordset
    
    On Error GoTo Errores
    Set mRs = New ADODB.Recordset
    If ExisteRsActualizar(pNombre) Then
        mRs.Open pNombre, "Provider=MSPersist;"
        If Not mRs.EOF Then
            If ExisteCampoRs(mRs, "ID") Then
                mRs.Sort = "id " & IIf(pAsc, "", " desc ")
            Else
                If ExisteCampoRs(mRs, "n_id") Then mRs.Sort = "n_id" & IIf(pAsc, "", " desc ")
            End If
        End If
        Set AbrirRsActualizar = mRs
    End If
    Exit Function
Errores:
    Set AbrirRsActualizar = New ADODB.Recordset
    Err.Clear
End Function

Public Function RsContieneDatos(pRs As ADODB.Recordset) As Boolean
    If Not pRs Is Nothing Then
        RsContieneDatos = Not pRs.EOF
    End If
End Function

Public Function BuscarMaxId(pRsPend As ADODB.Recordset) As Long
    pRsPend.Sort = "id desc "
    BuscarMaxId = pRsPend!id
End Function

Public Function ExisteCampoRs(pRs As ADODB.Recordset, pCampo As String) As Boolean
    On Error GoTo Errores
    a = pRs.Fields(pCampo).Value
    ExisteCampoRs = True
    Exit Function
Errores:
    Err.Clear
End Function

Public Function ExisteRsActualizar(pNombre As String) As Boolean
    ExisteRsActualizar = Dir(pNombre, vbArchive) <> ""
End Function

Public Sub MoverArchivo(Ruta As String, Archivo As String, Procesado As Boolean)
    
    Dim mCarpetaDestino As String
    
    mCarpetaDestino = App.Path & "\" & IIf(Procesado, "Procesado", "NoProcesado")
    
    If Dir(mCarpetaDestino, vbDirectory) = "" Then
        MkDir mCarpetaDestino
    End If
    
    If Dir(Ruta & "\" & Archivo, vbArchive) <> "" Then
        MoveFile Ruta & "\" & Archivo, mCarpetaDestino & "\" & Archivo
    End If
    
End Sub

Function PopStreamOfDb(RecSet As ADODB.Recordset, Campo As Variant) As Object
    
    On Error GoTo Errores
    
    Dim LibreArc As Integer
    Dim FS, f, s, counter As Long
    
    Set PopStreamOfDb = Nothing
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    sTemp = "StellarProductPicTmp.jpg"
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    If FS.FileExists(sTemp) Then
        KillSecure sTemp
    End If
    
    LibreArc = FreeFile()
    
    Open sTemp For Append Access Write As #LibreArc
    
    If Not RecSet.EOF Then
        Buffer = RecSet.Fields(Campo).GetChunk(RecSet.Fields(Campo).ActualSize)
    End If
    
    Print #LibreArc, Buffer
    
    Close LibreArc

    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    Set m_stream = New ADODB.Stream
    
    If FS.FileExists(sTemp) Then
        
        m_stream.Type = adTypeBinary
        m_stream.Open
        m_stream.LoadFromFile sTemp
        
        'ObjImg.Picture = LoadPicture(STemp)
        Set f = FS.GetFile(sTemp)
        f.Delete
        
    End If
    
    Set PopStreamOfDb = m_stream
    
    Exit Function
    
Errores:
    
    If LibreArc <> 0 Then Close LibreArc
    
End Function

Public Function PopImageOfDbChunk(ByRef ObjImg As Object, RecSet As ADODB.Recordset, Campo As Variant, _
Optional ByVal TmpImgFileName As Variant, Optional isPNGFileFormat As Boolean = False) As Boolean
    
    On Error GoTo ErrLoad
    
    Dim LibreArc As Integer
    Dim FS, f, s, counter As Long
    Dim WritingFile As Boolean
    
    PopImageOfDbChunk = False
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    If IsMissing(TmpImgFileName) Then
        sTemp = "STELLAR_BK001.TMP"
    Else
        sTemp = TmpImgFileName
    End If
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    KillSecure sTemp
    
    LibreArc = FreeFile()
    
    Open sTemp For Append Access Write As #LibreArc
    
    WritingFile = True
    
    If Not RecSet.EOF Then
        Buffer = RecSet.Fields(Campo).GetChunk(RecSet.Fields(Campo).ActualSize)
    End If
    
    Print #LibreArc, Buffer
    
    Close LibreArc
    
    WritingFile = False
    
    If isPNGFileFormat Then
        LoadFileToControl TmpForm.Pic, sTemp
        ObjImg.Picture = TmpForm.Pic.Picture
    Else
        ObjImg.Picture = LoadPicture(sTemp)
    End If
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
    If FS.FileExists(sTemp) And IsMissing(TmpImgFileName) Then
        KillSecure sTemp
    End If
    
    PopImageOfDbChunk = True
    
    Exit Function
    
ErrLoad:
    
    If WritingFile Then Close LibreArc
    
    EscribirLog "Error al cargar la imagen en la ruta [" & sTemp & "][" & Err.Description & "][" & Err.Number & "][" & CStr(WritingFile) & "]"
    
    ObjImg.Picture = LoadPicture
    
End Function

Public Function PopImageOfDbStream(ByRef ObjImg As Object, RecSet As ADODB.Recordset, Campo As Variant, _
Optional ByVal TmpImgFileName As Variant, Optional isPNGFileFormat As Boolean = False) As Boolean
    
    On Error GoTo ErrLoad
    
    Dim LibreArc As Integer
    Dim FS, f, s, counter As Long
    Dim WritingFile As Boolean
    
    PopImageOfDbStream = False
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    If IsMissing(TmpImgFileName) Then
        sTemp = "STELLAR_BK001.TMP"
    Else
        sTemp = TmpImgFileName
    End If
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    KillSecure sTemp
    
    If Not RecSet.EOF Then
        
        Dim TmpFileStream As ADODB.Stream
        
        Set TmpFileStream = New ADODB.Stream
        
        TmpFileStream.Type = adTypeBinary
        TmpFileStream.Open
        
        WritingFile = True
        
        TmpFileStream.Write RecSet.Fields(Campo).Value
        
        TmpFileStream.SaveToFile sTemp, adSaveCreateOverWrite
        TmpFileStream.Close
        
        WritingFile = False
        
    End If
    
    If isPNGFileFormat Then
        LoadFileToControl TmpForm.Pic, sTemp
        ObjImg.Picture = TmpForm.Pic.Picture
    Else
        ObjImg.Picture = LoadPicture(sTemp)
    End If
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
    If FS.FileExists(sTemp) And IsMissing(TmpImgFileName) Then
        KillSecure sTemp
    End If
    
    PopImageOfDbStream = True
    
    Exit Function
    
ErrLoad:
    
    If WritingFile And Not TmpFileStream Is Nothing Then
        If TmpFileStream.State <> adStateClosed Then
            TmpFileStream.Close
        End If
    End If
    
    EscribirLog "Error al cargar la imagen en la ruta [" & sTemp & "][" & Err.Description & "][" & Err.Number & "][" & CStr(WritingFile) & "]"
    
    ObjImg.Picture = LoadPicture
    
End Function

Public Function QuitarComillasDobles(pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    On Error Resume Next
    SafeEquals = (pVal1 = pVal2) ' Attempt to Compare Values.
    If Err.Number = 0 Then Exit Function
    SafeEquals = (pVal1 Is pVal2) ' Attempt to Compare Object References.
End Function

' Changed AS a Pointer to V2.

Public Function Collection_AddKey(pCollection As Collection, ByVal pValor, ByVal pKey As String) As Boolean
    Collection_AddKey = Collection_SafeAddKey(pCollection, pValor, pKey)
End Function

' To attempt to add Item + Key and ignore error in case Key is already contained in the collection.
' Ignore duplication error.

Public Function Collection_SafeAddKey(pCollection As Collection, ByVal pValue, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Add pValue, pKey
    
    Collection_SafeAddKey = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_EncontrarValor(pCollection As Collection, ByVal pValor, Optional ByVal pIndiceInicial As Long = 1) As Long
    Collection_EncontrarValor = Collection_FindValueIndex(pCollection, pValor, pIndiceInicial)
End Function

' How to Use:

' 1) To Know if Value Exists:

'If Collection_FindValueIndex(pCollection, pValue) <> -1 Then

' 2) To Search All Instances (Indexes) of Value in the Collection. Quick Example:

' SearchIndex = Collection_FindValueIndex(pCollection, pValue) ' Start Looking from Index 1

' If SearchIndex <> -1 Then
    ' This should be in a kind of While Loop...
    ' Keep searching other matches starting from next position:
    'SearchIndex = Collection_FindValueIndex(pCollection, pValue, SearchIndex + 1)
    ' Until Search Index = -1
' Else
    ' Next Match not found...
' End if

Public Function Collection_FindValueIndex(pCollection As Collection, ByVal pValue, Optional ByVal pStartIndex As Long = 1) As Long
    
    On Error GoTo Error
    
    Dim i As Long
    
    Collection_FindValueIndex = -1
    
    For i = pStartIndex To pCollection.Count
        If SafeEquals(pCollection.Item(i), pValue) Then
            Collection_FindValueIndex = i
            Exit Function
        End If
    Next i
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteKey(pCollection As Collection, ByVal pKey As String, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteKey = Collection_HasKey(pCollection, pKey)
End Function

' Collection_ExisteKey V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pKey, Null)) Then ' Key might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pKey) ' If this doesn't Proc Error Key is Null, but exists.
        Collection_HasKey = True
    Else ' Key Exists AND <> NULL
        Collection_HasKey = True
    End If
    
    Exit Function
        
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasKey = False ' Only on error, Key is not contained in the Collection.
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteIndex(pCollection As Collection, ByVal pIndex As Long, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteIndex = Collection_HasIndex(pCollection, pIndex)
End Function

' Collection_ExisteIndex V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pIndex, Null)) Then ' Index might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pIndex) ' If this doesn't Proc Error Index also exists.
        Collection_HasIndex = True
    Else ' Index Exists AND <> NULL
        Collection_HasIndex = True
    End If
    
    Exit Function
    
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasIndex = False ' Only on error Index is not contained in the Collection.
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean
    Collection_RemoveKey = Collection_SafeRemoveKey(pCollection, pKey)
End Function

' Ignore error even if the Key is not contained in the Collection.

Public Function Collection_SafeRemoveKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pKey
        
    Collection_SafeRemoveKey = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveKey = False ' Non - existant.
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    Collection_RemoveIndex = Collection_SafeRemoveIndex(pCollection, pIndex)
End Function

' Ignore error even if the Index is not contained in the Collection.

Public Function Collection_SafeRemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pIndex
        
    Collection_SafeRemoveIndex = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveIndex = False ' Non - existant.
    
End Function

Public Function SafeSavePicture(pCtl, ByVal pFilePath) As Boolean
    On Error GoTo ErrSave
    SavePicture TmpForm.Img.Picture, pFilePath
    SafeSavePicture = True
    Exit Function
ErrSave:
End Function

Public Function GetFileProp(ByVal FilePath As String, ByVal Property As String) As Variant
    
    On Error GoTo ErrFileProperties
    
    Dim FSO As New FileSystemObject, TmpFile As File, TmpVar As Variant
    
    Set TmpFile = FSO.GetFile(FilePath)
        
    Select Case UCase(Property)
        Case UCase("Type")
            GetFileProp = TmpFile.Type
        Case UCase("Extension")
            TmpVar = Strings.Split(TmpFile.Path, ".")
            GetFileProp = "." & TmpVar(UBound(TmpVar))
        Case UCase("Description")
            GetFileProp = Null
        Case UCase("Size")
            GetFileProp = TmpFile.Size
    End Select
    
    Exit Function
    
ErrFileProperties:
    
    GetContentType = Null
    
End Function

Public Function KillSecure(ByVal pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(ByVal pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

Public Function LoadTextFile(ByVal pFilePath As String) As String
    
    On Error GoTo Error
    
    TmpForm.RDoc.LoadFile pFilePath, rtfText
    LoadTextFile = TmpForm.RDoc.Text
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

Public Function SaveTextFile(ByVal pFilePath As String, ByVal pFileContents As String) As Boolean
    
    On Error GoTo Error
    
    TmpForm.RDoc.Text = pFileContents
    TmpForm.RDoc.SaveFile pFilePath, rtfText
    SaveTextFile = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function


Public Function PathExists(pPath As String) As Boolean
    'On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStr(1, pPath, "\")
    
    If Pos <> 0 Then
        GetDirectoryRoot = Left(pPath, Pos - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStrRev(pPath, "\")
    
    If Pos <> 0 Then
        GetDirParent = Left(pPath, Pos - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For i = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(i)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next i
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function


' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            isDBNull = pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Function ConvertirCadenaDeAsociacion(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|") As Collection
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacion = New Collection
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        ParClaveValor = Split(CStr(Item), ":", 2)               'Izq:Clave        'Der:Valor
        Call Collection_SafeAddKey(ConvertirCadenaDeAsociacion, ParClaveValor(1), ParClaveValor(0))
    Next
    
    If ConvertirCadenaDeAsociacion.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacion = Nothing
    
End Function

Public Function ConvertirCadenaDeAsociacionAvanzado(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":", _
Optional UCaseKeys As Boolean = False, _
Optional LCaseKeys As Boolean = False, _
Optional UCaseValue As Boolean = False, _
Optional LCaseValue As Boolean = False) As Dictionary
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacionAvanzado = New Dictionary
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(Item), pSeparadorInterno, 2) ' Izq -> Clave ' Der -> Valor
        
        If UCaseKeys Then
            ParClaveValor(0) = UCase(ParClaveValor(0))
        ElseIf LCaseKeys Then
            ParClaveValor(0) = LCase(ParClaveValor(0))
        End If
        
        If UCaseValue Then
            ParClaveValor(1) = UCase(ParClaveValor(1))
        ElseIf LCaseValue Then
            ParClaveValor(1) = LCase(ParClaveValor(1))
        End If
        
        If Not ConvertirCadenaDeAsociacionAvanzado.Exists(ParClaveValor(0)) Then
            ConvertirCadenaDeAsociacionAvanzado.Add ParClaveValor(0), ParClaveValor(1)
        End If
        
    Next
    
    If ConvertirCadenaDeAsociacionAvanzado.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacionAvanzado = Nothing
    
End Function
