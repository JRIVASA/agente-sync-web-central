VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsConexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mConexion                                           As ADODB.Connection
Private mvarServidor                                        As String
Private mvarUsuario                                         As String
Private mvarClave                                           As String
Private mvarBD                                              As String
Private mvarCnnTO                                           As Long
Private mvarCmdTO                                           As Long
Private mvarProveedor                                       As String

Property Get Conexion() As ADODB.Connection
    Set Conexion = mConexion
End Property

Public Sub IniciarClase(Srv As String, Optional BD As String = "VAD20", Optional User As String = "sa", Optional Clave As String = "", Optional CnnTO As Long = 15, Optional CmdTo As Long = 30, Optional Proveedor As String = "SQLOLEDB.1")
    mvarServidor = Srv
    mvarUsuario = User
    mvarBD = BD
    mvarClave = Clave
    mvarCnnTO = CnnTO
    mvarCmdTO = CmdTo
    mvarProveedor = Proveedor
End Sub

Public Sub AsignarCredenciales(mUser As String, mPass As String)
    mvarUsuario = mUser
    mvarClave = mPass
End Sub

'Public Function ConectarBD() As Boolean
'    On Error GoTo Errores
'    Set mConexion = New ADODB.Connection
'    mConexion.ConnectionTimeout = mvarCnnTO
'    mConexion.Open CadenaConexion
'    ConectarBD = True
'    Exit Function
'Errores:
'    EscribirLog Err.Description & ",Conectando " & mvarServidor
'    Err.Clear
'End Function

Public Function ConectarBDSucursal() As Boolean
    
Retry:
    
    On Error GoTo Errores
    
    Set mConexion = New ADODB.Connection
    mConexion.ConnectionTimeout = mvarCnnTO
    mConexion.Open CadenaConexion
    ConectarBDSucursal = True
    
    GoTo Finally
    
Errores:
    
    If Err.Number = -2147217843 Then
                
        Dim mClsTmp As Object
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If mClsTmp Is Nothing Then GoTo Otros
        
        MsgBox "Los datos de acceso para la conexi�n al Servidor de la Sucursal no estan establecidos o son incorrectos. Se le solicitar�n a continuaci�n."
        
        TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
        
        If Not IsEmpty(TmpVar) Then
            mUserCnSucursal = TmpVar(0): mPassCnSucursal = TmpVar(1)
            EscribirConfiguracion "SRV_LOCAL", "User", TmpVar(2)
            EscribirConfiguracion "SRV_LOCAL", "PWD", TmpVar(3)
            AsignarCredenciales mUserCnSucursal, mPassCnSucursal
            Resume Retry
        Else
            EscribirLog "Los datos de acceso para la conexi�n al servidor son incorrectos, Conectando " & mvarServidor
            Exit Function
        End If
        
    End If
    
Otros:
    
    EscribirLog Err.Description & ",Conectando " & mvarServidor
    Err.Clear
    
Finally:
    
    mUserCnSucursal = Empty
    mPassCnSucursal = Empty
    
End Function

Public Function ConectarBDWeb() As Boolean
    
Retry:
    
    On Error GoTo Errores
    
    Set mConexion = New ADODB.Connection
    mConexion.ConnectionTimeout = mvarCnnTO
    mConexion.Open CadenaConexion
    ConectarBDWeb = True
    
    GoTo Finally
    
Errores:
    
    If Err.Number = -2147217843 Then
                
        Dim mClsTmp As Object
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If mClsTmp Is Nothing Then GoTo Otros
        
        MsgBox "Los datos de acceso para la conexi�n al Servidor Web no estan establecidos o son incorrectos. Se le solicitar�n a continuaci�n."
        
        TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
        
        If Not IsEmpty(TmpVar) Then
            mUserCnWeb = TmpVar(0): mPassCnWeb = TmpVar(1)
            EscribirConfiguracion "SRV_WEB", "User", TmpVar(2)
            EscribirConfiguracion "SRV_WEB", "PWD", TmpVar(3)
            AsignarCredenciales mUserCnWeb, mPassCnWeb
            Resume Retry
        Else
            EscribirLog "Los datos de acceso para la conexi�n al servidor son incorrectos, Conectando " & mvarServidor
            Exit Function
        End If
        
    End If
    
Otros:
    
    EscribirLog Err.Description & ",Conectando " & mvarServidor
    Err.Clear
    
Finally:
    
    mUserCnWeb = Empty
    mPassCnWeb = Empty
    
End Function

Public Function CadenaConexion() As String
    CadenaConexion = "PROVIDER=" & mvarProveedor & ";INITIAL CATALOG=" & mvarBD & ";USER ID=" & mvarUsuario _
    & ";PASSWORD=" & mvarClave & ";DATA SOURCE=" & mvarServidor
End Function

Public Function ExisteCampoRs(Rs As ADODB.Recordset, Campo As String) As Boolean
    
    Dim mField As ADODB.Field
    
    For Each mField In Rs.Fields
        If StrComp(mField.Name, Campo, vbTextCompare) = 0 Then
            ExisteCampoRs = True
            Exit Function
        End If
    Next
    
End Function

Public Function CopiarCamposRecordset(ByVal pRsSource As ADODB.Recordset, ByRef pRsTarget As ADODB.Recordset, pFieldException As Variant) As Boolean
    Dim mField As ADODB.Field
    
    On Error GoTo Errores
    For Each mField In pRsSource.Fields
        If ExisteCampoRs(pRsTarget, mField.Name) Then
            If ValidarNombreCampo(pFieldException, mField.Name) Then
                pRsTarget.Fields(mField.Name).Value = mField.Value
            End If
        End If
    Next
    CopiarCamposRecordset = True
    Exit Function
Errores:
    EscribirLog Err.Description & ",Copiando Campos Recordset"
    
End Function

Public Function CopiarRecordsetIguales(ByVal pRsSource As ADODB.Recordset, ByRef pRsTarget As ADODB.Recordset, pFieldException As Variant) As Boolean
    Dim mField As ADODB.Field
    
    On Error GoTo Errores
    Do While Not pRsSource.EOF
        pRsTarget.AddNew
        If Not CopiarCamposRecordset(pRsSource, pRsTarget, pFieldException) Then
            pRsTarget.CancelUpdate
            GoTo Errores
        End If
        pRsTarget.Update
        pRsSource.MoveNext
    Loop
    CopiarRecordsetIguales = True
    Exit Function
Errores:
    EscribirLog Err.Description & ",Copiando Recordset"
End Function

Public Function ContieneDatosRecordset(pRs As ADODB.Recordset) As Boolean
    If Not pRs Is Nothing Then
        ContieneDatosRecordset = Not pRs.EOF
    End If
End Function

Private Function ValidarNombreCampo(pCampos, pCampo) As Boolean
    
    For mCampo = 0 To UBound(pCampos)
        If UCase(pCampos(mCampo)) = UCase(pCampo) Then
            Exit Function
        End If
    Next mCampo
    
    ValidarNombreCampo = True
    
End Function

Public Function Correlativo(Campo As String, Optional Sumar As Boolean = True) As Double
    
    Dim mRs As ADODB.Recordset
    Dim mSql As String
    
    mSql = "SELECT * FROM MA_CORRELATIVOS " & _
    "WHERE cu_Campo = '" & Campo & "'"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSql, mConexion, adOpenStatic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        If Sumar Then mRs!nu_valor = mRs!nu_valor + 1
        Correlativo = mRs!nu_valor
        mRs.Update
    Else
        mRs.AddNew
            mRs!cu_campo = Campo
            mRs!nu_valor = IIf(Sumar, 1, 0)
            mRs!cu_formato = "0000000000"
        mRs.Update
        Correlativo = 1
    End If
    
End Function

Public Function CorrelativoWeb(Campo As String, Optional Sumar As Boolean = True) As Double
    
    On Error GoTo ErrAgente
    
    Dim mRs As ADODB.Recordset
    Dim mSql As String
    
    mSql = "SELECT * FROM SYSTEM_CORRELATIVES " & _
    "WHERE Identifier = '" & Campo & "'"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSql, mConexion, adOpenStatic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        If Sumar Then mRs!Value = mRs!Value + 1
        CorrelativoWeb = mRs!Value
        mRs.Update
    Else
        mRs.AddNew
            mRs!Identifier = Campo
            mRs!Value = IIf(Sumar, 1, 0)
            mRs!Format = "0000000000"
        mRs.Update
        CorrelativoWeb = 1
    End If
    
    Exit Function
    
ErrAgente:
    
    EscribirLog "Error al obtener correlativo. No se realiz� ninguna operaci�n."
    End
    
End Function
