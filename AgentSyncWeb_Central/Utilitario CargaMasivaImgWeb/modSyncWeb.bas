Attribute VB_Name = "modSyncWeb"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Private Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" _
    (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long
    
Private Const ArchivoLog As String = "isAgentSyncWeb.log"
Private Const ArchivoIni As String = "Setup.ini"

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Public Enum Enum_FieldType
    Numerico
    Alfanumerico
End Enum

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

' Advertencia: NO Cambiar ninguno de los 3 par�metros anteriores. Para efectos de resguardo de las
' credenciales de acceso a SQL se esta utilizando la misma combinaci�n de par�metros de Stellar BUSINESS.
' Para que las claves encriptadas sean las mismas en todos los Agentes de Sincronizaci�n Web.
' Despues que las claves hayan sido generadas si esto se llegara a cambiar no funcionar�a la conexi�n SQL
' Al desplegar una nueva versi�n.

Global mUserCnSucursal As String
Global mPassCnSucursal As String

Global mUserCnWeb As String
Global mPassCnWeb As String

Global DebugMode        As Boolean

Public Function ObtenerConfiguracion(ByVal SSection As String, ByVal SKey As String, ByVal SDefault As String) As String
    Dim sTemp As String * 10000
    Dim nLength As Integer
    
    sTemp = Space$(10000)
    nLength = GetPrivateProfileString(SSection, SKey, SDefault, sTemp, 9999, App.Path & "\" & ArchivoIni)
    ObtenerConfiguracion = Left$(sTemp, nLength)
End Function

Public Sub EscribirConfiguracion(ByVal SSection As String, ByVal SKey As String, ByVal SDefault As String)
    WritePrivateProfileString SSection, SKey, SDefault, App.Path & "\" & ArchivoIni
End Sub

Public Sub EscribirLog(ByVal Msg As String)
    
    Dim mCanal As Integer
    
    On Error Resume Next
    
    mCanal = FreeFile()
    
    Dim TmpArchivo As String
    
    TmpArchivo = App.Path & "\" & ArchivoLog
    
    Open TmpArchivo For Append Access Write As #mCanal
    Print #mCanal, Now & ", " & Msg
    Close #mCanal
    
    Err.Clear
    
End Sub

Sub Main()
    
    If App.PrevInstance Then
        End
    End If
    
    gPK = Chr(55) & Chr(81) & Chr(76) & "_" _
    & Chr(73) & Chr(55) & Chr(51) & Chr(51) _
    & Chr(83) & Chr(78) & Chr(75) & Chr(52) _
    & Chr(78) & Chr(50) & Chr(69) & Chr(74) _
    & Chr(68) & Chr(49) & Chr(83) & "-" _
    & Chr(66) & Chr(44) & Chr(66) & Chr(71) _
    & Chr(52) & Chr(44) & Chr(90) & "_" & Chr(71)
    
    DebugMode = Dir(App.Path & "\DebugMode.Active") <> vbNullString
    
    Dim mCls As clsSyncWeb
    
    Set mCls = New clsSyncWeb
    mCls.IniciarAgente
    Set mCls = Nothing
    
End Sub

Public Function NombreArchivo(pTabla As String, pCorrelativo As String, Optional pCarpeta As String = "") As String
    
    Dim mDirectorio As String
    
    mDirectorio = IIf(pCarpeta <> "", pCarpeta, App.Path) & "\"
    NombreArchivo = mDirectorio & pTabla & ";" & pCorrelativo & ".adtg"
    'MsgBox NombreArchivo
    
End Function

Public Function BuscarArchivosActualizar(pRutaBusqueda As String, Optional Pedidos As Boolean) As Collection
    
    Dim mArchivos As Collection
    Dim mArchivo As String
    
    Set mArchivos = New Collection
    
    If Pedidos Then
        mArchivo = Dir(pRutaBusqueda & "\RSMP*.adtg")
    Else
        mArchivo = Dir(pRutaBusqueda & "\*pend*.adtg")
    End If
    
    Do While mArchivo <> ""
        mArchivos.Add mArchivo
        mArchivo = Dir
    Loop
    
    Set BuscarArchivosActualizar = mArchivos
    
End Function

Public Function AbrirRsActualizar(pNombre As String, Optional pAsc As Boolean = True) As ADODB.Recordset
    Dim mRs As ADODB.Recordset
    
    On Error GoTo Errores
    Set mRs = New ADODB.Recordset
    If ExisteRsActualizar(pNombre) Then
        mRs.Open pNombre, "Provider=MSPersist;"
        If Not mRs.EOF Then
            If ExisteCampoRs(mRs, "ID") Then
                mRs.Sort = "id " & IIf(pAsc, "", " desc ")
            Else
                If ExisteCampoRs(mRs, "n_id") Then mRs.Sort = "n_id" & IIf(pAsc, "", " desc ")
            End If
        End If
        Set AbrirRsActualizar = mRs
    End If
    Exit Function
Errores:
    Set AbrirRsActualizar = New ADODB.Recordset
    Err.Clear
End Function

Public Function RsContieneDatos(pRs As ADODB.Recordset) As Boolean
    If Not pRs Is Nothing Then
        RsContieneDatos = Not pRs.EOF
    End If
End Function

Public Function ExisteCampoRs(pRs As ADODB.Recordset, pCampo As String) As Boolean
    On Error GoTo Errores
    A = pRs.Fields(pCampo).Value
    ExisteCampoRs = True
    Exit Function
Errores:
    Err.Clear
End Function

Public Function ExisteRsActualizar(pNombre As String) As Boolean
    ExisteRsActualizar = Dir(pNombre, vbArchive) <> ""
End Function

Public Sub MoverArchivo(Ruta As String, Archivo As String, Procesado As Boolean)
    
    Dim mCarpertaDestino As String
    
    mCarpertaDestino = App.Path & "\" & IIf(Procesado, "Procesado", "NoProcesado")
    
    If Dir(mCarpertaDestino, vbDirectory) = Empty Then
        MkDir mCarpertaDestino
    End If
    
    If Dir(Ruta & "\" & Archivo, vbArchive) <> "" Then
        MoveFile Ruta & "\" & Archivo, mCarpertaDestino & "\" & Archivo
    End If
    
End Sub

Public Function QuitarComillasDobles(pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

'Funciones auxiliares para trabajar con Collections.

' Changed AS a Pointer to V2.

Public Function Collection_AddKey(pCollection As Collection, ByVal pValor, ByVal pKey As String) As Boolean
    Collection_AddKey = Collection_SafeAddKey(pCollection, pValor, pKey)
End Function

' To attempt to add Item + Key and ignore error in case Key is already contained in the collection.
' Ignore duplication error.

Public Function Collection_SafeAddKey(pCollection As Collection, ByVal pValue, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Add pValue, pKey
    
    Collection_SafeAddKey = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

'Public Function Collection_IgualA(pCollection As Collection, pValor) As Boolean
'
'    On Error GoTo Err
'
'    Dim i As Long
'
'    For i = 1 To pCollection.Count
'        If pCollection.Item(i) = pValor Then Collection_IgualA = True: Exit Function
'    Next i
'
'    Exit Function
'
'Err:
'
'    Debug.Print Err.Description
'
'    Collection_IgualA = False
'
'End Function

Public Function Collection_EncontrarValor(pCollection As Collection, pValor, Optional pIndiceStart As Long = 1) As Long
    
    On Error GoTo Err
    
    Dim i As Long
    
    For i = pIndiceStart To pCollection.Count
        If pCollection.Item(i) = pValor Then Collection_EncontrarValor = i: Exit Function
    Next i
    
    Collection_EncontrarValor = -1
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_EncontrarValor = -1
    
End Function

Public Function Collection_ExisteKey(pCollection As Collection, pKey As String, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pKey)
    Else
        Set tmpValorObj = pCollection.Item(pKey)
    End If
     
    Collection_ExisteKey = True
     
    Exit Function
        
Err:
    
    'Debug.Print Err.Description
    
    Collection_ExisteKey = False
    
End Function

Public Function Collection_ExisteIndex(pCollection As Collection, pIndex As Long, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pIndex)
    Else
        Set tmpValorObj = pCollection.Item(pIndex)
    End If
     
    Collection_ExisteIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_ExisteIndex = False
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveKey = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveKey = False
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveIndex = False
    
End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function ExisteCampoTabla(ByVal pColumna As String, ByVal pTabla As String, pCn As ADODB.Connection, _
Optional ByVal pBD As String = "") As Boolean
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Column_Name FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS WHERE Table_Name = '" & pTabla & "' AND Column_Name = '" & pColumna & "'")
    ExisteCampoTabla = Not (pRs.EOF And pRs.BOF)
End Function

Public Function BuscarReglaNegocioStr(pCampo, _
Optional pDefault As String = "", _
Optional pCn As ADODB.Connection) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSql As String
    
    On Error GoTo Errores
    
    mSql = "SELECT * FROM MA_REGLASDENEGOCIO WHERE Campo = '" & pCampo & "'"
    
    mRs.Open mSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarReglaNegocioStr = CStr(mRs!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.Open SQL, pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD = mRs.Fields(Campo).Value
    Else
        BuscarValorBD = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    Err.Clear
    BuscarValorBD = pDefault
    
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
   
   Dim HowManyLines As Integer
   
   HowManyLines = HowMany
   
   For i = 1 To HowManyLines
       GetLines = GetLines & vbNewLine
   Next i
   
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
   
   Dim HowManyTabs As Integer
   
   HowManyTabs = HowMany
   
   For i = 1 To HowManyTabs
       GetTab = GetTab & vbTab
   Next i
   
End Function

Public Function ExisteTabla(ByVal pTabla As String, _
pCn As ADODB.Connection, _
Optional ByVal pBD As String = "") As Boolean
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTabla = Not (pRs.EOF And pRs.BOF)
End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = Empty Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Function ConvertirCadenaDeAsociacion(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|") As Collection
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacion = New Collection
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        ParClaveValor = Split(CStr(Item), ":", 2)               'Izq:Clave        'Der:Valor
        Call Collection_SafeAddKey(ConvertirCadenaDeAsociacion, ParClaveValor(1), ParClaveValor(0))
    Next
    
    If ConvertirCadenaDeAsociacion.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacion = Nothing
    
End Function

Public Function ConvertirCadenaDeAsociacionAvanzado(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":") As Dictionary
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacionAvanzado = New Dictionary
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        ParClaveValor = Split(CStr(Item), pSeparadorInterno, 2) ' Izq -> Clave ' Der -> Valor
        If Not ConvertirCadenaDeAsociacionAvanzado.Exists(ParClaveValor(0)) Then
            ConvertirCadenaDeAsociacionAvanzado.Add ParClaveValor(0), ParClaveValor(1)
        End If
    Next
    
    If ConvertirCadenaDeAsociacionAvanzado.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacionAvanzado = Nothing
    
End Function

Public Function CrearCampoTabla(ByVal pTabla As String, ByVal pCampo As String, _
ByVal pTipoDato As String, pCn As ADODB.Connection, _
Optional ByVal pValorDefault As String = Empty, _
Optional ByVal pPermiteNULL As Boolean = False, _
Optional ByVal pIdDefault As String = "*") As Boolean
    
    On Error Resume Next
    
    Dim mBD As String: mBD = IIf(IsEmpty(pBD), Empty, pBD & ".")
    
    Dim SQL As String: SQL = _
    "IF NOT EXISTS(SELECT * FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" & pTabla & "' AND COLUMN_NAME = '" & pCampo & "')" & vbNewLine & _
    "BEGIN" & vbNewLine & _
    "   ALTER TABLE dbo." & pTabla & " ADD " & pCampo & " " & pTipoDato & " " & _
    IIf(pPermiteNULL, "NULL", "NOT NULL") & IIf(pIdDefault = "*", _
    " " & "CONSTRAINT" & " " & pTabla & "_DEFAULT_" & pCampo, IIf(IsEmpty(pIdDefault), Empty, " " & pIdDefault)) & _
    IIf(IsEmpty(pValorDefault), Empty, " " & "DEFAULT" & " " & pValorDefault) & vbNewLine & _
    "   ALTER TABLE dbo." & pTabla & " " & "SET (LOCK_ESCALATION = TABLE)" & vbNewLine & _
    "   PRINT 'TABLA ACTUALIZADA'" & vbNewLine & _
    "END"
    
    pCn.Execute SQL, Success
    
    If Err.Number = 0 Then CrearCampoTabla = True
    
End Function

Public Function CampoLength(Campo As String, Tabla As String, Server_BD As ADODB.Connection, _
FieldType As Enum_FieldType, _
Optional MinLength As Integer = 1) As Integer
      
    On Error GoTo FUAAA
    
    Dim RsLength As New ADODB.Recordset

    RsLength.Open "SELECT " & Campo & " FROM " & Tabla & " WHERE 1 = 2", _
    Server_BD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Select Case FieldType
    
    Case Enum_FieldType.Alfanumerico
        CampoLength = RsLength.Fields(Campo).DefinedSize
    Case Enum_FieldType.Numerico
        CampoLength = RsLength.Fields(Campo).Precision
    Case Else
        CampoLength = RsLength.Fields(Campo).DefinedSize
    End Select
    
    If CampoLength < MinLength Then CampoLength = MinLength
    
    RsLength.Close
    
    Exit Function

FUAAA:

    Debug.Print Err.Description
    CampoLength = MinLength
    
End Function

Public Function FixTSQL(ByVal pCadena As String) As String
    FixTSQL = Replace(pCadena, "'", "''")
End Function
